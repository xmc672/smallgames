/*
 * Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
 * Licensed under GNU GPLv3; see the README and the file "COPYING" for details.
 */
package net.benjamin_boehme.smallgames;

import net.benjamin_boehme.smallgames.Utils.Player;
import net.benjamin_boehme.smallgames.Utils.Token;

public abstract class Game {
	protected Board board;
	protected boolean isOverFlag;
	protected int currentTurn;
	protected Player currentPlayer; // must not be DRAW!
	protected Player winner; // expected to be null as long as isOverFlag==false

	protected Game() {
		this.isOverFlag = false;
		this.currentTurn = 1;
		this.currentPlayer = Player.Player1;
		this.winner = null;
	}

	/*
	 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * Getter methods
	 */

	/**
	 * Returns the current state of the board.
	 * @return a 2d array of type Token
	 */
	public Token[][] getBoard() {
		return this.board.getGrid();
	}

	/**
	 * Checks whether the current game is over yet.
	 * @return <code>TRUE</code> if the game is over (i.e. if there is a winner
	 * or if there are no empty fields anymore) and <code>FALSE</code> otherwise.
	 */
	public boolean isOver() {
		return this.isOverFlag;
	}

	/**
	 * Returns the current turn. In the n-th turn, n-1 stones have already been
	 * set and the n-th stone is about to be set.
	 * @return
	 */
	public int getCurrentTurn() {
		return this.currentTurn;
	}

	/**
	 * Returns the current player. </br>
	 *
	 * When implementing a UI instance that is shared by two human players,
	 * this method can be used to display information about whose turn it is.
	 * @return One of <code>Player1</code>, <code>Player2</code>.
	 */
	public Player getCurrentPlayer() {
		return this.currentPlayer;
	}

	/**
	 * Returns the winner of the game.
	 *
	 * Should only be called after verifying that {@link #isOver()}
	 * returns <code>TRUE</code> to avoid nullpointer exceptions.
	 * @return One of <code>Player1</code>, <code>Player2</code>, <code>DRAW</code> if the game is over,
	 * or <code>NULL</code> if the game is not over yet.
	 */
	public Player getWinner() {
		return this.winner;
	}

	/*
	 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * Setters
	 */

	// TODO: change to setStone(Move) soon
	public abstract boolean setStone(int pos);
	public abstract boolean setStone(int row, int col);

	/*
	 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * Internal/auxiliary methods
	 */

	/**
	 * Updates the game: determine winner, whether game is over, new current player.
	 */
	protected void update() {
		if (this.board.existsBingo(this.currentPlayer)) {
			endGameWithWinner(this.currentPlayer);
		}
		else if (isFinalRound()) {
			endGameWithWinner(Player.DRAW);
		}
		else {
			switchToNextTurn();
		}
	}

	protected void endGameWithWinner(Player p) {
		this.winner = p;
		this.isOverFlag = true;
	}

	protected abstract boolean isFinalRound();

	protected void switchToNextTurn() {
		this.switchCurrentPlayer();
		this.currentTurn += 1;
	}

	/**
	 * Switches the current player from {@code Player1} to {@code Player2} or vice versa.
	 * @throws IllegalStateException if the current player is neither {@code Player1} nor {@code Player2}
	 */
	protected void switchCurrentPlayer() {
		if (this.currentPlayer == Player.Player1) {
			this.currentPlayer = Player.Player2;
		}
		else if (this.currentPlayer == Player.Player2) {
			this.currentPlayer = Player.Player1;
		}
		else {
			throw new IllegalStateException("No current player assigned.");
		}
	}
}
