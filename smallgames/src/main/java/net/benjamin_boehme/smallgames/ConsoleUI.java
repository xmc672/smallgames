/*
 * Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
 * Licensed under GNU GPLv3; see the README and the file "COPYING" for details.
 */
package net.benjamin_boehme.smallgames;

import java.util.Scanner;

import net.benjamin_boehme.smallgames.Utils.Player;

public abstract class ConsoleUI {
	protected Game game;
	protected Scanner in;
	protected int playerInput;

	protected ConsoleUI() {
		this.in = new Scanner(System.in);
	}

	public void start() {
		printWelcomeMessage();
		while (! this.game.isOver()) {
			printBoard();
			handleCurrentTurn();
		}
		printBoard();
		printWinner();
		tearDown();
	}

	protected abstract void printWelcomeMessage();

	protected void printBoard() {
		printBoardCaption();
		makeBoardAndInstructions();
		printBoardAndInstructions();
		System.out.println("");
	}

	protected abstract void printBoardCaption();
	protected abstract void makeBoardAndInstructions();
	protected abstract void printBoardAndInstructions();

	protected void handleCurrentTurn() {
		while (true) {
			getPlayerMove();
			if (applyPlayerMove()) {
				break;
			}
		}
	}

	protected abstract void getPlayerMove();

	protected void promptPlayerMove() {
		String currentPlayerString = this.game.getCurrentPlayer().toString();
		System.out.println(currentPlayerString + ", make your move:");
	}

	protected boolean applyPlayerMove() {
		if (this.game.setStone(this.playerInput)) {
			printMoveSuccessful();
			return true;
		}
		else {
			printMoveInvalid();
			return false;
		}
	}

	protected abstract void printMoveSuccessful();
	protected abstract void printMoveInvalid();

	protected void printWinner() {
		if (this.game.getWinner() == Player.DRAW) {
			System.out.println("The game ends in a draw.");
		}
		else {
			System.out.println("The winner is: " + this.game.getWinner());
		}
	}

	protected void tearDown() {
		this.in.close();
	}
}
