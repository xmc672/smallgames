/*
 * Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
 * Licensed under GNU GPLv3; see the README and the file "COPYING" for details.
 */
package net.benjamin_boehme.smallgames;

import net.benjamin_boehme.smallgames.Utils.Token;

class T3Board extends Board {
	public static final int HEIGHT = 3;
	public static final int WIDTH = 3;

	T3Board() {
		super(HEIGHT, WIDTH);
	}

	/**
	 * Constructor from 3x3 int array - only used for testing.
	 * @param grid Must be a 3x3 integer array that only contains
	 * entries 0, 1, 2.
	 * @throws IllegalArgumentException if <code>grid</code> has the
	 * wrong dimensions or if <code>grid</code> has entries that are
	 * not equal to 0, 1, 2.
	 */
	T3Board(int[][] grid) {
		super(grid, HEIGHT, WIDTH);
	}

	/*
	 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * Evaluation methods
	 */

	/**
	 * Checks whether there exists a bingo (3-in-a-row) for the given token.
	 * @param t The player's {@code Token}
	 * @return
	 */
	boolean existsBingo(Token t) {
		// check rows
		for (int i = 0; i < super.grid.length; i++) {
			if (super.grid[i][0] == t && super.grid[i][1] == t && super.grid[i][2] == t) {
				return true;
			}
		}

		// check cols
		for (int j = 0; j < super.grid.length; j++) {
			if (super.grid[0][j] == t && super.grid[1][j] == t && super.grid[2][j] == t) {
				return true;
			}
		}

		// check diagonal
		if (super.grid[0][0] == t && super.grid[1][1] == t && super.grid[2][2] == t) {
			return true;
		}

		// check anti-diagonal
		if (super.grid[0][2] == t && super.grid[1][1] == t && super.grid[2][0] == t) {
			return true;
		}

		return false;
	}
}
