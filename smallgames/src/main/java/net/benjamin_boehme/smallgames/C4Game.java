/*
 * Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
 * Licensed under GNU GPLv3; see the README and the file "COPYING" for details.
 */
package net.benjamin_boehme.smallgames;

import net.benjamin_boehme.smallgames.Utils.Token;

public class C4Game extends Game {
	private C4Game() {
		super();
		this.board = new C4Board();
	}

	/**
	 * Factory method that returns a new instance of C4Game.
	 * This game
	 * - is not over yet,
	 * - has Player1 as current player,
	 * - has no winner yet, and
	 * - has an empty board.
	 * @return
	 */
	public static C4Game newC4Game() {
		return new C4Game();
	}

	/*
	 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * Setter methods
	 */

	/**
	 * Tries to set a stone for the current player at the given board position
	 * and updates the state of the game.
	 * <p>
	 * Tries to insert the current player's token in the given column of the
	 * board and returns {@code true/false} depending on whether or not this
	 * operation was successful. This method must only be called after the game
	 * was initialized, and the arguments must specify a valid column.
	 * Setting a stone succeeds if the given column of the board still has empty
	 * fields, and fails if the column is completely filled with tokens.
	 * <br>
	 * If successful, the method also updates the state of the game: it checks
	 * <ul>
	 *   <li>if the current player has won and the game should therefore end
	 *   with the current player as the winner,</li>
	 *   <li>if there are no more possible moves and the game should therefore
	 *   end in a draw,</li>
	 *   <li>if there still exist valid moves and the game should therefore
	 *   go on.</li>
	 * </ul>
	 * <p>
	 * It is the UI's responsibility to call this method with the correct user
	 * input from the correct player at the right time, i.e. after the game
	 * started and before it is over.
	 *
	 * @param col an integer in the range 0-6, '0' being the leftmost column
	 * @return {@code true} if setting a stone on the given field was
	 * successful, {@code false} if the field is already taken
	 * @throws IllegalStateException if no current player is assigned, i.e. if
	 * the game was not properly initialized by calling {@link #start()},
	 * or if the game is already over.
	 * @throws IllegalArgumentException if {@code col} is not in the range 0-6.
	 */
	@Override
	public boolean setStone(int col) {
		if (this.currentPlayer == null) {
			throw new IllegalStateException("No current player assigned.");
		}
		else if (this.isOverFlag) {
			throw new IllegalStateException("The game is over.");
		}
		else if (col < 0 || col > C4Board.WIDTH-1) {
			throw new IllegalArgumentException(String.format("%d is not a valid column.", col));
		}

		for (int row = C4Board.HEIGHT-1; row >= 0; row--) {
			if (this.board.getField(row, col) == Token.EMPTY) {
				Token t = Token.fromPlayer(this.currentPlayer); // current player's Token
				this.board.setField(t, row, col);
				this.update();
				return true;
			}
		}
		return false;
	}

	// ad-hoc implementation of abstract method; replace later
	@Override
	public boolean setStone(int row, int col) {
		return this.setStone(col);
	}

	protected boolean isFinalRound() {
		return this.currentTurn >= C4Board.HEIGHT * C4Board.WIDTH;
	}
}
