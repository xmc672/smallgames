/*
 * Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
 * Licensed under GNU GPLv3; see the README and the file "COPYING" for details.
 */
package net.benjamin_boehme.smallgames;

import java.util.Scanner;

public class Utils {
	/*
	 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * Enums
	 */
	public enum Player {
		 Player1(1), Player2(2), DRAW(3);

		private int id;

		private Player(int n) {
			this.id = n;
		}

		/**
		 * Get a player from its ID.
		 * @param n Must be one of 1 (=Player1), 2 (=Player2), 3 (=DRAW).
		 * @return The corresponding player.
		 * @throws IllegalArgumentException if the argument is not equal to 1, 2, 3.
		 */
		public static Player buildFromId(int n) {
			switch (n) {
			case 1:
				return Player1;
			case 2:
				return Player2;
			case 3:
				return DRAW;
			default:
				throw new IllegalArgumentException("Argument must equal one of 1, 2, 3!");
			}
		}

		/**
		 * Returns the ID of a <code>Player</code>.
		 * @return Returns 1 for Player1, 2 for Player2, 3 for DRAW.
		 */
		public int getId() {
			return this.id;
		}
	}

	public enum Token {
		EMPTY(0), Player1(1), Player2(2);

		private int id;

		private Token(int n) {
			this.id = n;
		}

		/**
		 * Returns the ID of a <code>Token</code>.
		 * @return Returns 0 for EMPTY, 1 for Player1, 2 for Player2.
		 */
		public int getId() {
			return this.id;
		}

		/**
		 * Get a token from its ID.
		 * @param n Must be one of 0 (=EMPTY), 1 (=Player1), 2 (=Player2).
		 * @return The corresponding token.
		 * @throws IllegalArgumentException if the argument is not equal to 0, 1, 2.
		 */
		public static Token fromId(int n) {
			switch (n) {
			case 0:
				return EMPTY;
			case 1:
				return Player1;
			case 2:
				return Player2;
			default:
				throw new IllegalArgumentException("Argument must equal one of 0, 1, 2!");
			}
		}

		/**
		 * Get a token from a given {@code Player}.
		 * @param p Must be one of {@code Player1, Player2}.
		 * @return The corresponding token.
		 * @throws IllegalArgumentException if the argument is equal to {@code DRAW}.
		 */
		public static Token fromPlayer(Player p) {
			switch (p) {
			case Player1:
				return Player1;
			case Player2:
				return Player2;
			default:
				throw new IllegalArgumentException("Argument must equal one of Player1, Player2!");
			}
		}

		/**
		 * Convert an int array into a Token array.
		 * @param arr Must be an int array only containing
		 * 0 (=EMPTY), 1 (=Player1), 2 (=Player2).
		 * @return
		 * @throws IllegalArgumentException if there are entries outside of the
		 * range 0-2.
		 */
		public static Token[] arrayFromIntArray(int[] arr) {
			Token[] t = new Token[arr.length];
			for (int i = 0; i < arr.length; i++) {
				if (arr[i] < 0 | arr[i] > 2) {
					throw new IllegalArgumentException("All entries must be 0, 1, 2.");
				}
				t[i] = Token.fromId(arr[i]);
			}
			return t;
		}

		/**
		 * Convert a 2d int array into a 2d Token array.
		 * @param arr Must be a 2d int array only containing
		 * 0 (=EMPTY), 1 (=Player1), 2 (=Player2).
		 * @return
		 * @throws IllegalArgumentException if there are entries outside of the
		 * range 0-2.
		 */
		public static Token[][] matrixFromIntMatrix(int[][] matrix) {
			Token[][] t = new Token[matrix.length][matrix[0].length];
			for (int i = 0; i < t.length; i++) {
				t[i] = Token.arrayFromIntArray(matrix[i]);
			}
			return t;
		}

		/**
		 * Convert this Token to a string according to a conversion rule.
		 * Converts the Token EMPTY (i=0)/Player1 (i=1)/Player2 (i=2) to the
		 * length-1 string {@code conversionRule[i]}.
		 *
		 * Example: {@code Player1.toString(".XO"} returns 'X'.
		 * @param conversionRule A string of length 3.
		 * @return
		 * @throws IllegalArgumentException if {@code conversionRule} is not of
		 * length 3.
		 */
		public String toString(String conversionRule) {
			if (conversionRule.length() != 3) {
				throw new IllegalArgumentException("Conversion rule must be of length 3.");
			}
			int i = this.getId();
			return conversionRule.substring(i, i+1);
		}

		/**
		 * Convert a {@code Token} array into a String array using a conversion
		 * rule, see {@link #toString(String)} for details.
		 * @param array
		 * @param conversionRule A string of length 3.
		 * @return
		 * @throws IllegalArgumentException if {@code conversionRule} is not of
		 * length 3.
		 */
		public static String[] stringArrayFromArray(Token[] array, String conversionRule) {
			if (conversionRule.length() != 3) {
				throw new IllegalArgumentException("Conversion rule must be of length 3.");
			}
			String[] stringArr = new String[array.length];
			for (int i = 0; i < stringArr.length; i++) {
				stringArr[i] = array[i].toString(conversionRule);
			}
			return stringArr;
		}

		/**
		 * Convert a {@code Token} matrix into a String matrix using a conversion
		 * rule, see {@link #toString(String)} for details.
		 * @param matrix
		 * @param conversionRule A string of length 3.
		 * @return
		 * @throws IllegalArgumentException if {@code conversionRule} is not of
		 * length 3.
		 */
		public static String[][] stringMatrixFromMatrix(Token[][] matrix, String conversionRule) {
			if (conversionRule.length() != 3) {
				throw new IllegalArgumentException("Conversion rule must be of length 3.");
			}
			String[][] stringMatrix = new String[matrix.length][matrix[0].length];
			for (int i = 0; i < stringMatrix.length; i++) {
				stringMatrix[i] = Token.stringArrayFromArray(matrix[i], conversionRule);
			}
			return stringMatrix;
		}
	}

	/**
	 * Read an integer from a given {@code Scanner}.
	 * @param in
	 * @return
	 */
	public static int integerConsoleInput(Scanner in) {
		while (true) {
			if (in.hasNextInt()) {
				return in.nextInt();
			}
			else {
				System.out.println("\nInput needs to be an integer.");
				in.nextLine(); // advance Scanner past non-integer input
			}
		}
	}

	/**
	 * Repeatedly read an integer from a given {@code Scanner} until it is in a
	 * given range.
	 * @param in
	 * @param min The smallest acceptable return value.
	 * @param max The greatest acceptable return value.
	 * @return
	 */
	public static int integerConsoleInput(Scanner in, int min, int max) {
		int n;
		while (true) {
			n = Utils.integerConsoleInput(in);
			if (n >= min && n <= max) {
				return n;
			}
			else {
				String warning = String.format(
						"Please enter an integer in the range %d-%d.", min, max);
				System.out.println(warning);
			}
		}
	}
}
