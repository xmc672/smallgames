/*
 * Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
 * Licensed under GNU GPLv3; see the README and the file "COPYING" for details.
 */
package net.benjamin_boehme.smallgames;

import java.util.Scanner;

public class SmallGames 
{
	private int selectedGame;

	SmallGames() {
		this.selectedGame = 0;
	}

    public static void main( String[] args )
    {
        new SmallGames().run();
    }

    private void run() {
        printStartupMessage();
        promptSelectedGame();
        getSelectedGame();
        startSelectedGame();
    }

	private static void printStartupMessage() {
		System.out.println("Running 'SmallGames', a collection of small games\n");
	}

	private static void promptSelectedGame() {
		System.out.println("Select a game:");
		System.out.println("  1: Connect Four");
		System.out.println("  2: Tic Tac Toe");
	}

	private void getSelectedGame() {
		Scanner in = new Scanner(System.in);
		this.selectedGame = Utils.integerConsoleInput(in, 1, 2);
	}

	private void startSelectedGame() {
		switch (this.selectedGame) {
		case 1:
			new C4ConsoleUI().start();
			break;
		case 2:
			new T3ConsoleUI().start();
			break;
		default:
			throw new IllegalArgumentException("Invalid selection!");
		}
	}
}
