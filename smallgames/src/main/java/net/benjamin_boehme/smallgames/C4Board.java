/*
 * Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
 * Licensed under GNU GPLv3; see the README and the file "COPYING" for details.
 */
package net.benjamin_boehme.smallgames;

import net.benjamin_boehme.smallgames.Utils.Token;

class C4Board extends Board {
	public static final int HEIGHT = 6;
	public static final int WIDTH = 7;

	C4Board() {
		super(HEIGHT, WIDTH);
	}

	/**
	 * Constructor from 6x7 int array - only used for testing.
	 * @param grid Must be a 6x7 integer array that only contains
	 * entries 0, 1, 2.
	 * @throws IllegalArgumentException if <code>grid</code> has the
	 * wrong dimensions or if <code>grid</code> has entries that are
	 * not equal to 0, 1, 2.
	 */
	C4Board(int[][] grid) {
		super(grid, HEIGHT, WIDTH);
	}

	/*
	 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * Evaluation methods
	 */

	/**
	 * Checks whether there exists a bingo (4-in-a-row) for the given token.
	 * @param t The player's {@code Token}
	 * @return
	 */
	boolean existsBingo(Token t) {
		// check rows
		for (int i = 0; i < super.grid.length; i++) {
			for (int j = 0; j < 4; j++) {
				if (super.grid[i][j] == t && super.grid[i][j+1] == t && super.grid[i][j+2] == t && super.grid[i][j+3] == t) {
					return true;
				}
			}
		}

		// check cols
		for (int j = 0; j < 7; j++) {
			for (int i = 0; i < 3; i++) {
				if (super.grid[i][j] == t && super.grid[i+1][j] == t && super.grid[i+2][j] == t && super.grid[i+3][j] == t) {
					return true;
				}
			}
		}

		// check diags of shape '\'
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 4; j++) {
				if (super.grid[i][j] == t && super.grid[i+1][j+1] == t && super.grid[i+2][j+2] == t && super.grid[i+3][j+3] == t) {
					return true;
				}
			}
		}

		// check diags of shape '/'
		for (int i = 5; i > 2; i--) {
			for (int j = 0; j < 4; j++) {
				if (super.grid[i][j] == t && super.grid[i-1][j+1] == t && super.grid[i-2][j+2] == t && super.grid[i-3][j+3] == t) {
					return true;
				}
			}
		}

		return false;
	}
}
