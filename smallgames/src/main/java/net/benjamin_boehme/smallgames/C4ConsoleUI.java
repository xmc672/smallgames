/*
 * Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
 * Licensed under GNU GPLv3; see the README and the file "COPYING" for details.
 */
package net.benjamin_boehme.smallgames;

import net.benjamin_boehme.smallgames.Utils.Token;

public class C4ConsoleUI extends ConsoleUI {
	private String[] boardAndInstructions;
	private String[] labelsAndInstructions;
	private String[] printableBoardLines;

	public C4ConsoleUI() {
		this.game = C4Game.newC4Game();
		this.labelsAndInstructions = new String[6];
		makeLabelsAndInstructions();
		this.printableBoardLines = new String[6];
		this.boardAndInstructions = new String[6];
	}

	private void makeLabelsAndInstructions() {
		this.labelsAndInstructions[0] = "  'X' = Player1      %s";
		this.labelsAndInstructions[1] = "  'O' = Player2      %s";
		this.labelsAndInstructions[2] = "  '.' = empty        %s";
		this.labelsAndInstructions[3] = "                     %s";
		this.labelsAndInstructions[4] = " Enter a digit to    %s";
		this.labelsAndInstructions[5] = " play that column!   %s";
	}

	@Override
	protected void printWelcomeMessage() {
		System.out.println("Playing a new game of Connect Four...\n");
	}

	@Override
	protected void printBoardCaption() {
		System.out.println("\n  Labels/Controls    0 1 2 3 4 5 6");
		System.out.println("++++++++++++++++++++++++++++++++++++");
	}

	@Override
	protected void makeBoardAndInstructions() {
		makePrintableBoardLines();
		for (int i = 0; i < this.boardAndInstructions.length; i++) {
			this.boardAndInstructions[i] = String.format(this.labelsAndInstructions[i], this.printableBoardLines[i]);
		}
	}

	private void makePrintableBoardLines() {
		Token[][] board = this.game.getBoard();
		String[][] stringMatrix = Token.stringMatrixFromMatrix(board, ".XO");
		for (int i = 0; i < this.printableBoardLines.length; i++) {
			this.printableBoardLines[i] = String.format("%s %s %s %s %s %s %s",
					stringMatrix[i][0], stringMatrix[i][1], stringMatrix[i][2],
					stringMatrix[i][3], stringMatrix[i][4], stringMatrix[i][5],
					stringMatrix[i][6]);
		}
	}

	@Override
	protected void printBoardAndInstructions() {
		for (int i = 0; i < this.boardAndInstructions.length; i++) {
			System.out.println(this.boardAndInstructions[i]);
		}
	}

	@Override
	protected void getPlayerMove() {
		promptPlayerMove();
		this.playerInput = Utils.integerConsoleInput(this.in, 0, 6);
	}

	@Override
	protected void printMoveSuccessful() {
		System.out.println(this.game.getCurrentPlayer().toString()
			+ " plays in column " + this.playerInput);
	}

	@Override
	protected void printMoveInvalid() {
		System.out.println("\n" + "Column " + this.playerInput + " is completely filled.");
	}
}
