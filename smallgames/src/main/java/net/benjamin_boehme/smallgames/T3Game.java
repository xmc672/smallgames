/*
 * Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
 * Licensed under GNU GPLv3; see the README and the file "COPYING" for details.
 */
package net.benjamin_boehme.smallgames;

import net.benjamin_boehme.smallgames.Utils.Token;

public class T3Game extends Game{
	private T3Game() {
		super();
		this.board = new T3Board();
	}

	/**
	 * Factory method that returns a new instance of T3Game.
	 * This game
	 * - is not over yet,
	 * - has Player1 as current player,
	 * - has no winner yet, and
	 * - has an empty board.
	 * @return
	 */
	public static T3Game newT3Game() {
		return new T3Game();
	}

	/*
	 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * Setter methods
	 */

	/**
	 * Tries to set a stone for the current player at the given board position
	 * and updates the state of the game.
	 * <p>
	 * Tries to put the current player's token on the given field of the board
	 * and returns {@code TRUE/FALSE} depending on whether or not this
	 * operation was successful. This method must only be called after the game
	 * was initialized, and the arguments must specify a valid board position.
	 * Setting a stone succeeds if the given field of the board is still empty,
	 * and fails if the field already has a token of one the two players.
	 * <br>
	 * If successful, the method also updates the state of the game: it checks
	 * <ul>
	 *   <li>if the current player has won and the game should therefore end
	 *   with the current player as the winner,</li>
	 *   <li>if there are no more possible moves and the game should therefore
	 *   end in a draw,</li>
	 *   <li>if there still exist valid moves and the game should therefore
	 *   go on.</li>
	 * </ul>
	 * <p>
	 * It is the UI's responsibility to call this method with the correct user
	 * input from the correct player at the right time, i.e. after the game
	 * started and before it is over.
	 *
	 * @param row an integer in the range 0-2, '0' being the top row
	 * @param col an integer in the range 0-2, '0' being the leftmost column
	 * @return {@code TRUE} if setting a stone on the given field was
	 * successful, {@code FALSE} if the field is already taken
	 * @throws IllegalStateException if no current player is assigned, i.e. if
	 * the game was not properly initialized by calling {@link #start()},
	 * or if the game is already over.
	 * @throws IllegalArgumentException if one of {@code row,col} is not in the range 1-3.
	 */
	@Override
	public boolean setStone(int row, int col) {
		if (this.currentPlayer == null) {
			throw new IllegalStateException("No current player assigned.");
		}
		else if (this.isOverFlag) {
			throw new IllegalStateException("The game is over.");
		}

		if (this.board.getField(row, col) == Token.EMPTY) {
			Token t = Token.fromPlayer(this.currentPlayer); // current player's Token
			this.board.setField(t, row, col);
			this.update();
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Tries to set a stone for the current player at the given board position
	 * and updates the state of the game.
	 * <p>
	 * This is the "NumPad coordinate" version of {@link #setStone(int, int)}; see
	 * there for detailed documentation.
	 * @param numpadCoord a board position in NumPad format, i.e. a digit in the range 1-9
	 * @return {@code TRUE} if setting a stone on the given field was
	 * successful, {@code FALSE} if the field is already taken
	 * @throws IllegalStateException if no current player is assigned, i.e. if
	 * the game was not properly initialized by calling {@link #start()},
	 * or if the game is already over.
	 * @throws IllegalArgumentException if {@code digit} is not in the range 1-9.
	 */
	@Override
	public boolean setStone(int numpadCoord) {
		int row = T3Game.rowFromNumpad(numpadCoord);
		int col = T3Game.colFromNumpad(numpadCoord);
		return this.setStone(row, col);
	}

	/**
	 * Calculates the row from a NumPad coordinate.
	 * @param digit a "NumPad coord"
	 * @return the row
	 */
	private static int rowFromNumpad(int digit) {
		if (digit == 7 || digit == 8 || digit == 9) {
			return 0;
		}
		else if (digit == 4 || digit == 5 || digit == 6) {
			return 1;
		}
		else if (digit == 1 || digit == 2 || digit == 3) {
			return 2;
		}
		else {
			throw new IllegalArgumentException("Digit must be in the range 1-9!");
		}
	}

	/**
	 * Calculates the column of a NumPad coordinate.
	 * @param digit a "NumPad coord"
	 * @return the col
	 */
	private static int colFromNumpad(int digit) {
		if (digit == 7 || digit == 4 || digit == 1) {
			return 0;
		}
		else if (digit == 8 || digit == 5 || digit == 2) {
			return 1;
		}
		else if (digit == 9 || digit == 6 || digit == 3) {
			return 2;
		}
		else {
			throw new IllegalArgumentException("Digit must be in the range 1-9!");
		}
	}

	protected boolean isFinalRound() {
		return this.currentTurn >= T3Board.HEIGHT * T3Board.WIDTH;
	}
}
