/*
 * Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
 * Licensed under GNU GPLv3; see the README and the file "COPYING" for details.
 */

package net.benjamin_boehme.smallgames;

import net.benjamin_boehme.smallgames.Utils.Player;
import net.benjamin_boehme.smallgames.Utils.Token;

public abstract class Board {
	/*
	 * Note: so far, all game boards have an underlying grid, but this might be
	 * subject to change in future versions. In particular, games like Nine
	 * Men's Morris are hard to implement via a grid.
	 * Ideas:
	 * - move field 'grid' to subclass GridBoard
	 * - new data structure based on graphs/maps graph->token
	 */
	protected Token[][] grid;

	protected Board() {
		this.grid = new Token[0][0];
	}

	/**
	 * Construct Board instance whose {@code grid} is an empty 2d Token
	 * array of size {@code height}*{@code width}.
	 * @param width The number of rows
	 * @param height The number of cols
	 */
	protected Board(int height, int width) {
		this.grid = new Token[height][width];
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				this.grid[i][j] = Token.EMPTY;
			}
		}
	}

	/**
	 * Constructor from Token array - only used for testing.
	 * @param grid Must be a Token array of size {@code height*width}.
	 * @param height The number of rows
	 * @param width The number of cols
	 * @throws IllegalArgumentException if <code>grid</code> has the
	 * wrong dimensions.
	 */
	Board(Token[][] grid, int height, int width) {
		// check for correct dimension (height*width)
		if(grid.length != height) {
			throw new IllegalArgumentException("Not of correct height!");
		}
		else {
			for (int i = 0; i < grid.length; i++) {
				if(grid[i].length != width) {
					throw new IllegalArgumentException("Not of correct width!");
				}
			}
		}
		this.grid = grid;
	}

	/**
	 * Constructor from int array - only used for testing.
	 * @param grid Must be an integer array of size {@code height*width} that
	 * only contains entries 0, 1, 2.
	 * @param height The number of rows
	 * @param width The number of cols
	 * @throws IllegalArgumentException if <code>grid</code> has the
	 * wrong dimensions or if <code>grid</code> has entries that are
	 * not equal to 0, 1, 2.
	 */
	Board(int[][] grid, int height, int width) {
		this(Token.matrixFromIntMatrix(grid), height, width);
	}

	/*
	 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * Object methods
	 */

	/**
	 * Return the grid as a one-line string.
	 * The format is subject to change, but roughly looks like this:
	 * Rows are separated by '/' where the first (think top-most row) has
	 * index 0. Each row is represented as a string of integers 0-2, where
	 *   0: empty field
	 *   1: Player1
	 *   2: Player2.
	 * Example: A T3Board might look like this:
	 *   121/212/012
	 * where the only occurrence of '0' is at the bottom left position of the
	 * board.
	 * @return the string description
	 */
	@Override
	public String toString() {
		String s = new String("");
		int len = this.grid.length;

		for (int i = 0; i < len; i++) {
			for (int j = 0; j < this.grid[i].length; j++) {
				s += String.valueOf(this.grid[i][j].getId());
			}

			if (i < len-1) {
				s += "/";
			}
		}

		return s;
	}

	/*
	 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * Getters
	 */

	/**
	 * Returns the state of the given field on the board.
	 * @param row an integer in the range {@code 0-(height-1)}
	 * @param col an integer in the range {@code 0-(width-1)}
	 * @return
	 * @throws IllegalArgumentException if {@code (row, col)} is not a valid
	 * board coordinate.
	 */
	public Token getField(int row, int col) {
		try {
			return this.grid[row][col];
		} catch (IndexOutOfBoundsException e) {
			throw new IllegalArgumentException(
					String.format("(%d, %d) is not a valid board coordinate.", row, col));
		}
	}

	/**
	 * Returns the current state of the board as a Token array.
	 * @return the board
	 */
	public Token[][] getGrid() {
		return this.grid.clone();
	}

	/*
	 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * Setters
	 */

	/**
	 * Sets the board field at position {@code (row,col)} to the given {@code
	 * Token}.
	 * @param t The token
	 * @param row
	 * @param col
	 * @return True if the method changed the board (i.e. if the field was not
	 * set to the given {@code Token} before.
	 * @throws IndexOutOfBoundsException if {@code (row, col)} doesn't specify a
	 * valid position on the board.
	 */
	public boolean setField(Token t, int row, int col) {
		// set field
		try {
			if (this.grid[row][col] == t) {
				return false;
			}
			else {
				this.grid[row][col] = t;
				return true;
			}
		} catch (IndexOutOfBoundsException e) {
			throw new IndexOutOfBoundsException(
					String.format("(%d, %d) is not a valid board coordinate.", row, col));
		}
	}

	/*
	 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * Evaluation methods
	 */

	/**
	 * Checks whether or not the board still has empty fields.
	 * @return
	 */
	boolean existsEmptyField() {
		for (int i = 0; i < this.grid.length; i++) {
			for (int j = 0; j < this.grid[i].length; j++) {
				if (this.grid[i][j] == Token.EMPTY) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Checks whether a winning condition is met for the given player.
	 * @param player
	 * @return
	 * @throws IllegalArgumentException if {@code player} is not 1 or 2.
	 */
	boolean existsBingo(int player) {
		if (player < 1 || player > 2) {
			throw new IllegalArgumentException("Player must be either 1 or 2.");
		}

		Token t = Token.fromId(player);
		return this.existsBingo(t);
	}

	/**
	 * Checks whether a winning condition is met for the given player.
	 * @param p
	 * @return
	 * @throws IllegalArgumentException if {@code p} is {@code DRAW}.
	 */
	boolean existsBingo(Player p) {
		if (p == Player.DRAW) {
			throw new IllegalArgumentException("Player must not be DRAW!");
		}

		Token t = Token.fromPlayer(p);
		return this.existsBingo(t);
	}

	/**
	 * Checks whether a winning condition is met for the given player.
	 * @param t The player's {@code Token}
	 * @return
	 */
	abstract boolean existsBingo(Token t);
}
