/*
 * Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
 * Licensed under GNU GPLv3; see the README and the file "COPYING" for details.
 */
package net.benjamin_boehme.smallgames;

import net.benjamin_boehme.smallgames.Utils.Token;

public class T3ConsoleUI extends ConsoleUI {
	private String[] boardAndInstructions;
	private String[] labelsAndInstructions;
	private String[] printableBoardLines;

	public T3ConsoleUI() {
		this.game = T3Game.newT3Game();
		this.playerInput = 0;
		this.labelsAndInstructions = new String[3];
		makeLabelsAndInstructions();
		this.printableBoardLines = new String[3];
		this.boardAndInstructions = new String[3];
	}

	private void makeLabelsAndInstructions() {
		this.labelsAndInstructions[0] = "  'X' = Player1        %s        7 8 9";
		this.labelsAndInstructions[1] = "  'O' = Player2        %s        4 5 6";
		this.labelsAndInstructions[2] = "  '.' = empty          %s        1 2 3";
	}

	@Override
	public void printWelcomeMessage() {
		System.out.println("Playing a new game of TicTacToe...\n");
	}

	@Override
	protected void printBoardCaption() {
		System.out.println("\n     Labels            Board       Controls");
		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++");
	}

	@Override
	protected void makeBoardAndInstructions() {
		makePrintableBoardLines();
		for (int i = 0; i < this.boardAndInstructions.length; i++) {
			this.boardAndInstructions[i] = String.format(this.labelsAndInstructions[i], this.printableBoardLines[i]);
		}
	}

	private void makePrintableBoardLines() {
		Token[][] board = this.game.getBoard();
		String[][] stringMatrix = Token.stringMatrixFromMatrix(board, ".XO");
		for (int i = 0; i < this.printableBoardLines.length; i++) {
			this.printableBoardLines[i] = String.format("%s %s %s", stringMatrix[i][0], stringMatrix[i][1], stringMatrix[i][2]);
		}
	}

	@Override
	protected void printBoardAndInstructions() {
		for (int i = 0; i < this.boardAndInstructions.length; i++) {
			System.out.println(this.boardAndInstructions[i]);
		}
	}

	@Override
	protected void getPlayerMove() {
		promptPlayerMove();
		this.playerInput = Utils.integerConsoleInput(this.in, 1, 9);
	}

	@Override
	protected void printMoveSuccessful() {
		System.out.println(this.game.getCurrentPlayer().toString()
			+ " plays at position " + this.playerInput);
	}

	@Override
	protected void printMoveInvalid() {
		System.out.println("\n" + "Field " + this.playerInput + " already taken.");
	}
}
