/*
 * Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
 * Licensed under GNU GPLv3; see the README and the file "COPYING" for details.
 */
package net.benjamin_boehme.smallgames;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import net.benjamin_boehme.smallgames.Utils.Player;
import net.benjamin_boehme.smallgames.Utils.Token;

public class T3BoardTest {
	private T3Board b_empty, b_draw;
	@Rule
	public ExpectedException exceptionRule = ExpectedException.none();;

	@Before
	public void setUp() throws Exception {
		b_empty = new T3Board();
		b_draw = new T3Board( new int[][] {{2, 1, 2}, {2, 1, 2}, {1, 2, 1}});
	}

	@After
	public void tearDown() throws Exception {
		b_empty = null;
		b_draw = null;
	}

	/*
	 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * Test constructors
	 */

	@Test
	public void testT3Board() {
		assertNotNull("Should not be null", b_empty);
	}

	@Test
	public void testT3BoardIntArrayArray() {
		assertNotNull("Should not be null", b_draw);
	}

	@Test
	public void exceptionTestT3BoardIntArrayArray_lessThanThreeRows() {
		exceptionRule.expect(IllegalArgumentException.class);
		exceptionRule.expectMessage("Not of correct height!");
		new T3Board(new int[][] {{0, 0, 0}, {0, 0, 0}});
	}

	@Test
	public void exceptionTestT3BoardIntArrayArray_moreThanThreeRows() {
		exceptionRule.expect(IllegalArgumentException.class);
		exceptionRule.expectMessage("Not of correct height!");
		new T3Board(new int[][] {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}});
	}

	@Test
	public void exceptionTestT3BoardIntArrayArray_lessThanThreeCols() {
		exceptionRule.expect(IllegalArgumentException.class);
		exceptionRule.expectMessage("Not of correct width!");
		new T3Board(new int[][] {{0, 0, 0}, {0, 0, 0}, {0, 0}});
	}

	@Test
	public void exceptionTestT3BoardIntArrayArray_moreThanThreeCols() {
		exceptionRule.expect(IllegalArgumentException.class);
		exceptionRule.expectMessage("Not of correct width!");
		new T3Board(new int[][] {{0, 0, 0}, {0, 0, 0}, {0, 0, 0, 0}});
	}

	@Test
	public void exceptionTestT3BoardIntArrayArray_hasEntryLessThanZero() {
		exceptionRule.expect(IllegalArgumentException.class);
		exceptionRule.expectMessage("All entries must be 0, 1, 2.");
		new T3Board(new int[][] {{0, 0, 0}, {0, 0, 0}, {0, 0, -1}});
	}

	@Test
	public void exceptionTestT3BoardIntArrayArray_hasEntryGreaterThanTwo() {
		exceptionRule.expect(IllegalArgumentException.class);
		exceptionRule.expectMessage("All entries must be 0, 1, 2.");
		new T3Board(new int[][] {{0, 0, 0}, {0, 0, 0}, {0, 0, 3}});
	}

	/*
	 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * Test getter methods
	 */

	@Test
	public void testToString( ) {
		assertEquals("Should be '212/212/121'.", "212/212/121", b_draw.toString());
	}

	@Test
	public void testGetGrid() {
		Token[][] expected = new Token[][] {{Token.Player2, Token.Player1, Token.Player2},
											{Token.Player2, Token.Player1, Token.Player2},
											{Token.Player1, Token.Player2, Token.Player1}};
		assertTrue("Should agree.", Arrays.deepEquals(expected, b_draw.getGrid()));
	}

	/*
	 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * Test setter methods
	 */

	@Test
	public void testSetFieldTokenIntInt() {
		assertTrue("Setting a stone changes the board.", b_empty.setField(Token.Player1, 1, 1));
		assertEquals("Should be '000/010/000'", "000/010/000", b_empty.toString());
		assertFalse("Setting a stone doesn't change the board.", b_empty.setField(Token.Player1, 1, 1));
		assertTrue("Setting a stone changes the board.", b_empty.setField(Token.Player2, 0, 2));
		assertEquals("Should be '002/010/000'", "002/010/000", b_empty.toString());
	}

	@Test
	public void exceptionTestSetFieldTokenIntInt_rowLessThanOne() {
		exceptionRule.expect(IndexOutOfBoundsException.class);
		exceptionRule.expectMessage("(-1, 1) is not a valid board coordinate.");
		b_empty.setField(Token.Player1, -1, 1);
	}

	@Test
	public void exceptionTestSetFieldTokenIntInt_rowGreaterThanThree() {
		exceptionRule.expect(IndexOutOfBoundsException.class);
		exceptionRule.expectMessage("(3, 1) is not a valid board coordinate.");
		b_empty.setField(Token.Player1, 3, 1);
	}

	@Test
	public void exceptionTestSetFieldTokenIntInt_colLessThanOne() {
		exceptionRule.expect(IndexOutOfBoundsException.class);
		exceptionRule.expectMessage("(1, -1) is not a valid board coordinate.");
		b_empty.setField(Token.Player1, 1, -1);
	}

	@Test
	public void exceptionTestSetFieldTokenIntInt_colGreaterThanThree() {
		exceptionRule.expect(IndexOutOfBoundsException.class);
		exceptionRule.expectMessage("(1, 3) is not a valid board coordinate.");
		b_empty.setField(Token.Player1, 1, 3);
	}

	/*
	 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * Test evaluation methods
	 */

	@Test
	public void testExistsEmptyField() {
		assertTrue("Empty board should have empty fields", b_empty.existsEmptyField());
		assertFalse("Full board should not have empty fields", b_draw.existsEmptyField());
	}

	@Test
	public void testExistsBingoInt() {
		assertFalse("Empty board shouldn't have bingo for Player1.", b_empty.existsBingo(1));
		assertFalse("Empty board shouldn't have bingo for Player2.", b_empty.existsBingo(2));
		assertFalse("Draw board shouldn't have bingo for Player1.", b_draw.existsBingo(1));
		assertFalse("Draw board shouldn't have bingo for Player2.", b_draw.existsBingo(2));

		// case: Player1 has bingo in middle row, Player2 doesn't have bingo
		T3Board b = new T3Board( new int[][] {{1, 2, 0}, {1, 1, 1}, {2, 0, 2}});
		assertTrue("Player1 should have bingo.", b.existsBingo(1));
		assertFalse("Player2 should not have bingo.", b.existsBingo(2));

		// case: Player2 has bingo in middle column, Player1 doesn't have bingo
		b = new T3Board( new int[][] {{2, 2, 1}, {1, 2, 0}, {0, 2, 1}});
		assertTrue("Player2 should have bingo.", b.existsBingo(2));
		assertFalse("Player1 should not have bingo.", b.existsBingo(1));

		// case: Player1 has bingo in diagonal, Player2 doesn't have bingo
		b = new T3Board( new int[][] {{1, 2, 2}, {2, 1, 1}, {0, 0, 1}});
		assertTrue("Player1 should have bingo.", b.existsBingo(1));
		assertFalse("Player2 should not have bingo.", b.existsBingo(2));

		// case: Player2 has bingo in anti-diagonal, Player1 doesn't have bingo
		b = new T3Board( new int[][] {{1, 1, 2}, {2, 2, 1}, {2, 0, 0}});
		assertTrue("Player2 should have bingo.", b.existsBingo(2));
		assertFalse("Player1 should not have bingo.", b.existsBingo(1));
	}

	@Test
	public void exceptionTestExistsBingoInt_argLessThanOne() {
		exceptionRule.expect(IllegalArgumentException.class);
		exceptionRule.expectMessage("Player must be either 1 or 2.");
		b_draw.existsBingo(0);
	}

	@Test
	public void exceptionTestExistsBingoInt_argGreaterThanTwo() {
		exceptionRule.expect(IllegalArgumentException.class);
		exceptionRule.expectMessage("Player must be either 1 or 2.");
		b_draw.existsBingo(3);
	}

	@Test
	public void testExistsBingoPlayer() {
		assertFalse("Empty board shouldn't have bingo for Player1.", b_empty.existsBingo(Player.Player1));
		assertFalse("Empty board shouldn't have bingo for Player2.", b_empty.existsBingo(Player.Player2));
		assertFalse("Draw board shouldn't have bingo for Player1.", b_draw.existsBingo(Player.Player1));
		assertFalse("Draw board shouldn't have bingo for Player2.", b_draw.existsBingo(Player.Player2));

		// case: Player1 has bingo in middle row, Player2 doesn't have bingo
		T3Board b = new T3Board( new int[][] {{1, 2, 0}, {1, 1, 1}, {2, 0, 2}});
		assertTrue("Player1 should have bingo.", b.existsBingo(Player.Player1));
		assertFalse("Player2 should not have bingo.", b.existsBingo(Player.Player2));

		// case: Player2 has bingo in middle column, Player1 doesn't have bingo
		b = new T3Board( new int[][] {{2, 2, 1}, {1, 2, 0}, {0, 2, 1}});
		assertTrue("Player2 should have bingo.", b.existsBingo(Player.Player2));
		assertFalse("Player1 should not have bingo.", b.existsBingo(Player.Player1));

		// case: Player1 has bingo in diagonal, Player2 doesn't have bingo
		b = new T3Board( new int[][] {{1, 2, 2}, {2, 1, 1}, {0, 0, 1}});
		assertTrue("Player1 should have bingo.", b.existsBingo(Player.Player1));
		assertFalse("Player2 should not have bingo.", b.existsBingo(Player.Player2));

		// case: Player2 has bingo in anti-diagonal, Player1 doesn't have bingo
		b = new T3Board( new int[][] {{1, 1, 2}, {2, 2, 1}, {2, 0, 0}});
		assertTrue("Player2 should have bingo.", b.existsBingo(Player.Player2));
		assertFalse("Player1 should not have bingo.", b.existsBingo(Player.Player1));
	}

	@Test
	public void exceptionTestExistsBingoPlayer_argEqualsDraw() {
		exceptionRule.expect(IllegalArgumentException.class);
		exceptionRule.expectMessage("Player must not be DRAW!");
		b_draw.existsBingo(Player.DRAW);
	}

}
