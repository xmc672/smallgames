/*
 * Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
 * Licensed under GNU GPLv3; see the README and the file "COPYING" for details.
 */
package net.benjamin_boehme.smallgames;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import net.benjamin_boehme.smallgames.Utils.Player;

public class PlayerTest {
	@Rule
	public ExpectedException exceptionRule = ExpectedException.none();;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testBuildFromId() {
		Player p;
		p = Player.buildFromId(1);
		assertEquals(Player.Player1, p);
		p = Player.buildFromId(2);
		assertEquals(Player.Player2, p);
		p = Player.buildFromId(3);
		assertEquals(Player.DRAW, p);
	}

	@Test
	public void exceptionTestBuildFromId_argLessThanOne() {
		exceptionRule.expect(IllegalArgumentException.class);
		exceptionRule.expectMessage("Argument must equal one of 1, 2, 3!");
		Player.buildFromId(0);
	}

	@Test
	public void exceptionTestBuildFromId_argGreaterThanThree() {
		exceptionRule.expect(IllegalArgumentException.class);
		exceptionRule.expectMessage("Argument must equal one of 1, 2, 3!");
		Player.buildFromId(4);
	}

	@Test
	public void testGetId() {
		assertEquals(Player.Player1.getId(), 1);
		assertEquals(Player.Player2.getId(), 2);
		assertEquals(Player.DRAW.getId(), 3);
	}

}
