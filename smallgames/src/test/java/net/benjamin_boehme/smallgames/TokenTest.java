/*
 * Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
 * Licensed under GNU GPLv3; see the README and the file "COPYING" for details.
 */
package net.benjamin_boehme.smallgames;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import net.benjamin_boehme.smallgames.Utils.Player;
import net.benjamin_boehme.smallgames.Utils.Token;

public class TokenTest {
	@Rule
	public ExpectedException exceptionRule = ExpectedException.none();

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetId() {
		assertEquals(Token.EMPTY.getId(), 0);
		assertEquals(Token.Player1.getId(), 1);
		assertEquals(Token.Player2.getId(), 2);
	}

	@Test
	public void testFromId() {
		Token t;
		t = Token.fromId(0);
		assertEquals(Token.EMPTY, t);
		t = Token.fromId(1);
		assertEquals(Token.Player1, t);
		t = Token.fromId(2);
		assertEquals(Token.Player2, t);
	}

	@Test
	public void exceptionTestFromId_argLessThanZero() {
		exceptionRule.expect(IllegalArgumentException.class);
		exceptionRule.expectMessage("Argument must equal one of 0, 1, 2!");
		Token.fromId(-1);
	}

	@Test
	public void exceptionTestFromId_argGreaterThanTwo() {
		exceptionRule.expect(IllegalArgumentException.class);
		exceptionRule.expectMessage("Argument must equal one of 0, 1, 2!");
		Token.fromId(3);
	}

	@Test
	public void testFromPlayer() {
		Token t;
		t = Token.fromPlayer(Player.Player1);
		assertEquals(Token.Player1, t);
		t = Token.fromPlayer(Player.Player2);
		assertEquals(Token.Player2, t);
	}

	@Test
	public void exceptionTestFromPlayer_argIsDraw() {
		exceptionRule.expect(IllegalArgumentException.class);
		exceptionRule.expectMessage("Argument must equal one of Player1, Player2!");
		Token.fromPlayer(Player.DRAW);
	}

	@Test
	public void testArrayFromIntArray() {
		int[] intArray = { 0, 1, 2};
		Token[] tokenArray = Token.arrayFromIntArray(intArray);
		assertEquals(Token.EMPTY, tokenArray[0]);
		assertEquals(Token.Player1, tokenArray[1]);
		assertEquals(Token.Player2, tokenArray[2]);
	}

	@Test
	public void exceptionTestArrayFromIntArray_someEntryLessThanZero() {
		exceptionRule.expect(IllegalArgumentException.class);
		exceptionRule.expectMessage("All entries must be 0, 1, 2.");
		int[] intArray = { -1, 1, 2};
		Token.arrayFromIntArray(intArray);
	}

	@Test
	public void exceptionTestArrayFromIntArray_someEntryGreaterThanTwo() {
		exceptionRule.expect(IllegalArgumentException.class);
		exceptionRule.expectMessage("All entries must be 0, 1, 2.");
		int[] intArray = { 0, 1, 3};
		Token.arrayFromIntArray(intArray);
	}

	@Test
	public void testMatrixFromIntMatrix() {
		int[][] intMatrix = { {0, 1, 2}, {1, 2, 0}, {2, 0, 1}};
		Token[][] tokenMatrix = Token.matrixFromIntMatrix(intMatrix);
		assertEquals(Token.EMPTY, tokenMatrix[0][0]);
		assertEquals(Token.Player2, tokenMatrix[1][1]);
		assertEquals(Token.Player1, tokenMatrix[2][2]);
	}

	@Test
	public void exceptionTestMatrixFromIntMatrix_someEntryLessThanZero() {
		exceptionRule.expect(IllegalArgumentException.class);
		exceptionRule.expectMessage("All entries must be 0, 1, 2.");
		int[][] intMatrix = { {0, 1, 2}, {-1, 2, 0}, {2, 0, 1}};
		Token.matrixFromIntMatrix(intMatrix);
	}

	@Test
	public void exceptionTestMatrixFromIntMatrix_someEntryGreaterThanTwo() {
		exceptionRule.expect(IllegalArgumentException.class);
		exceptionRule.expectMessage("All entries must be 0, 1, 2.");
		int[][] intMatrix = { {0, 1, 2}, {1, 2, 0}, {2, 0, 3}};
		Token.matrixFromIntMatrix(intMatrix);
	}

	@Test
	public void testToString() {
		String conversionRule = ".XO";
		String[] s = {
				Token.EMPTY.toString(conversionRule),
				Token.Player1.toString(conversionRule),
				Token.Player2.toString(conversionRule)};
		assertEquals(".", s[0]);
		assertEquals("X", s[1]);
		assertEquals("O", s[2]);
	}

	@Test
	public void exceptionTestToString_argTooShort() {
		exceptionRule.expect(IllegalArgumentException.class);
		exceptionRule.expectMessage("Conversion rule must be of length 3.");
		Token.EMPTY.toString(".X");
	}

	@Test
	public void exceptionTestToString_argTooLong() {
		exceptionRule.expect(IllegalArgumentException.class);
		exceptionRule.expectMessage("Conversion rule must be of length 3.");
		Token.EMPTY.toString(".XO?");
	}

	@Test
	public void testStringArrayFromArray() {
		String conversionRule = ".XO";
		Token[] array = {Token.EMPTY, Token.Player1, Token.Player2};
		String[] stringArr = Token.stringArrayFromArray(array, conversionRule);
		assertEquals(".", stringArr[0]);
		assertEquals("X", stringArr[1]);
		assertEquals("O", stringArr[2]);
	}

	@Test
	public void exceptionTestStringArrayFromArray_argTooShort() {
		String conversionRule = ".X";
		Token[] array = {Token.EMPTY, Token.Player1, Token.Player2};
		exceptionRule.expect(IllegalArgumentException.class);
		exceptionRule.expectMessage("Conversion rule must be of length 3.");
		Token.stringArrayFromArray(array, conversionRule);
	}

	@Test
	public void exceptionTestStringArrayFromArray_argTooLong() {
		String conversionRule = ".XO?";
		Token[] array = {Token.EMPTY, Token.Player1, Token.Player2};
		exceptionRule.expect(IllegalArgumentException.class);
		exceptionRule.expectMessage("Conversion rule must be of length 3.");
		Token.stringArrayFromArray(array, conversionRule);
	}

	@Test
	public void testStringMatrixFromMatrix() {
		String conversionRule = ".XO";
		Token[][] matrix = {{Token.EMPTY, Token.Player1}, {Token.Player2, Token.EMPTY}};
		String[][] stringMatrix = Token.stringMatrixFromMatrix(matrix, conversionRule);
		assertEquals(".", stringMatrix[0][0]);
		assertEquals("X", stringMatrix[0][1]);
		assertEquals("O", stringMatrix[1][0]);
	}

	@Test
	public void exceptionTestStringMatrixFromMatrix_argTooShort() {
		String conversionRule = ".X";
		Token[][] matrix = {{Token.EMPTY, Token.Player1}, {Token.Player2, Token.EMPTY}};
		exceptionRule.expect(IllegalArgumentException.class);
		exceptionRule.expectMessage("Conversion rule must be of length 3.");
		Token.stringMatrixFromMatrix(matrix, conversionRule);
	}

	@Test
	public void exceptionTestStringMatrixFromMatrix_argTooLong() {
		String conversionRule = ".XO?";
		Token[][] matrix = {{Token.EMPTY, Token.Player1}, {Token.Player2, Token.EMPTY}};
		exceptionRule.expect(IllegalArgumentException.class);
		exceptionRule.expectMessage("Conversion rule must be of length 3.");
		Token.stringMatrixFromMatrix(matrix, conversionRule);
	}
}
