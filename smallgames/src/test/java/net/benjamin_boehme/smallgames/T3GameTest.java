/*
 * Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
 * Licensed under GNU GPLv3; see the README and the file "COPYING" for details.
 */
package net.benjamin_boehme.smallgames;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import net.benjamin_boehme.smallgames.Utils.Player;
import net.benjamin_boehme.smallgames.Utils.Token;

public class T3GameTest {
	private T3Game game;
	@Rule
	public ExpectedException exceptionRule = ExpectedException.none();

	@Before
	public void setUp() throws Exception {
		game = T3Game.newT3Game();
	}

	@After
	public void tearDown() throws Exception {
		game = null;
	}

	/*
	 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * Test factory method
	 */

	@Test
	public void testNewT3Game() {
		assertFalse("Should not be over yet.", game.isOver());
		assertEquals("Current player should be Player1", game.getCurrentPlayer(), Player.Player1);
		assertNull("There should be no winner assigned.", game.getWinner());
		Token[][] board_matrix = game.getBoard();
		int[][] zero_matrix = new int[3][3];
		Token[][] empty_matrix = Token.matrixFromIntMatrix(zero_matrix);
		assertTrue("Should be completely empty.", Arrays.deepEquals(board_matrix, empty_matrix));
	}

	/*
	 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * Test getter methods
	 */

	@Test
	public void testGetBoard() {
		// skip testing for forwarding call
		assertTrue(true);
	}

	@Test
	public void testIsOver() {
		// skip testing for simple getter method
		assertTrue(true);
	}

	@Test
	public void testGetCurrentPlayer() {
		assertTrue(true);
	}

	@Test
	public void testGetWinner() {
		// skip testing for simple getter method
		assertTrue(true);
	}

	/*
	 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * Test setter methods
	 */

	@Test
	public void testSetStoneInt() {
		// We'll simulate a short game and use "NumPad coordinates" throughout.
		// Player1 goes first and plays at '6'.
		assertEquals("It should be Player1's turn.", Player.Player1, game.getCurrentPlayer());
		assertTrue(game.setStone(6));
		// Player2 should not be able to play at '6' anymore.
		assertFalse(game.isOver());
		assertEquals("It should be Player2's turn.", Player.Player2, game.getCurrentPlayer());
		assertFalse(game.setStone(6));
		// Player2 plays at '8'.
		assertFalse(game.isOver());
		assertEquals("It should be Player2's turn.", Player.Player2, game.getCurrentPlayer());
		assertTrue(game.setStone(8));
		// Player1 should not be able to play at '8' anymore.
		assertFalse(game.isOver());
		assertEquals("It should be Player1's turn.", Player.Player1, game.getCurrentPlayer());
		assertFalse(game.setStone(8));
		// Player1 plays at '5'.
		assertFalse(game.isOver());
		assertEquals("It should be Player1's turn.", Player.Player1, game.getCurrentPlayer());
		assertTrue(game.setStone(5));
		// Player2 makes a mistake and plays at '3'.
		assertFalse(game.isOver());
		assertEquals("It should be Player2's turn.", Player.Player2, game.getCurrentPlayer());
		assertTrue(game.setStone(3));
		// Player1 plays at '4'.
		assertFalse(game.isOver());
		assertEquals("It should be Player1's turn.", Player.Player1, game.getCurrentPlayer());
		assertTrue(game.setStone(4));
		// Now the game is over and Player1 has won.
		assertTrue(game.isOver());
		assertEquals("Player1 is the winner.", Player.Player1, game.getWinner());
		// Finally, we check that the board looks the right way.
		Token[][] expected = new Token[][] {{Token.EMPTY, Token.Player2, Token.EMPTY},
											{Token.Player1, Token.Player1, Token.Player1},
											{Token.EMPTY, Token.EMPTY, Token.Player2}};
		assertTrue(Arrays.deepEquals(expected, game.getBoard()));
	}

	@Test
	public void exceptionTestSetStoneInt_gameOver() {
		// play quick game
		game.setStone(7); // P1 @ 7
		game.setStone(1); // P2 @ 1
		game.setStone(8); // P1 @ 8
		game.setStone(2); // P2 @ 2
		game.setStone(9); // P1 @ 9
		// game over, next call of setStone triggers exception
		exceptionRule.expect(IllegalStateException.class);
		exceptionRule.expectMessage("The game is over.");
		game.setStone(3);
	}

	@Test
	public void exceptionTestSetStoneInt_digitLessThanOne() {
		exceptionRule.expect(IllegalArgumentException.class);
		exceptionRule.expectMessage("Digit must be in the range 1-9!");
		game.setStone(0);
	}

	@Test
	public void exceptionTestSetStoneInt_digitGreaterThanNine() {
		exceptionRule.expect(IllegalArgumentException.class);
		exceptionRule.expectMessage("Digit must be in the range 1-9!");
		game.setStone(10);
	}
}
